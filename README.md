**《Ziry正则表达式调试工具V1.1》** 


这个程序是用JAVA开发的，所以你操作系统需要安装好JAVA环境，

如果没有安装JAVA环境则请下载[jre8_86.zip](http://pan.baidu.com/s/1eSLs1Ku)，解压安装jar8_86.exe，

按指示安装完成后，双击运行：[ZiryRegularToolV1.1.jar](http://git.oschina.net/zirylee/RegularToolV1.1/attach_files)

![输入图片说明](http://git.oschina.net/zirylee/RegularToolV1.1/raw/master/images/show.jpg?dir=0&filepath=images%2Fshow.jpg&oid=0e278f9dfe628e238a83ee8f4dabf0bf55b4772b&sha=103a6a858f742fc30f8ec41c71213baca45dcaae "在这里输入图片标题")